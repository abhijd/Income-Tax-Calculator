// PL_tax_project.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include <conio.h>

float IncomeTaxMale ( float income );                            // Function to calculate tax of male 
float IncomeTaxFemaleSenior ( float income );                    // Function to calculate tax of female and seniors
float IncomeRetard ( float income );                             // Function to calculate tax of retard
int decide();

int _tmain()
{
	printf("\n\t\tTHIS IS A PROGRAM TO CALCULATE INCOME TAX\n\n");

	int opt,de;
	float income;
	
	do{
		printf("\n\tPress\n\
			   1: To enter your monthly income.\n\
			   2: To enter your annual income.\n\
			   3: If you need help in choosing.\n\
			   0: To QUIT.\n\n");
		opt=_getch();


		if(opt==49){
			printf("Please enter your monthly income: ");
			scanf_s("%f",&income);
			income=12*income;
			de=decide();
			printf("\n\nde==%d\n\n",de);


		}
		else if(opt==50){
			printf("2");
		}
		else if(opt==51){
			printf("3");
		}
		else if(opt<48 || opt>51){
			printf("\nINVALID CHOICE!! PLEASE TRY AGAIN.\n");
		}
	}while(opt!=48);
	printf("\n\nPROGRAM ENDED. Thank You.");
	return 0;
	
}

float IncomeTaxMale ( float income ){
float tax;
	if (income<=180000){
		tax=0;
	}
	else if(income<=480000){
		tax=(float)((income-180000)*0.1);
		if(tax<2000){
			tax=2000;
		}
	}
	else if (income<=880000){
		tax=(float)( (300000*0.1)+(  (income-480000)*0.15  )  );
		if(tax<2000){
			tax=2000;
		}
	}
	else if ( income<=1180000 ) {
		tax=(float) (  (300000*0.1) + (400000*0.15)+(  (income-880000)*0.20  )  );
		if(tax<2000){
			tax=2000;
		}
	}
	else if(income>1180000){
		tax=(float) (  (300000*0.1) + (400000*0.15)+(300000*0.20)+(  (income-1180000)* 0.25  )  );
		if(tax<2000){
			tax=2000;
		}
	}

	return tax;
}

float IncomeTaxFemaleSenior ( float income ){
float tax;
	if (income<=200000){
		tax=0;
	}
	else if( income<=500000 ){
		tax=(float)( (income-200000)*0.1 );
		if(tax<2000){
			tax=2000;
		}
	}
	else if ( income<=900000 ){
		tax=(float)( (300000*0.1)+(  (income-500000)*0.15  )  );
		if(tax<2000){
			tax=2000;
		}
	}
	else if ( income<=1200000 ) {
		tax=(float) (  (300000*0.1) + (400000*0.15)+(  (income-900000)*0.20  )  );
		if(tax<2000){
			tax=2000;
		}
	}
	else if( income>1200000 ){
		tax=(float) (  (300000*0.1) + (400000*0.15)+(300000*0.20)+(  (income-1200000)* 0.25  )  );
		if(tax<2000){
			tax=2000;
		}
	}

	return tax;
}

float IncomeRetard ( float income ){

float tax;
	if (income<=250000){
		tax=0;
	}
	else if( income<=550000 ){
		tax=(float)( (income-250000)*0.1 );
		if(tax<2000){
			tax=2000;
		}
	}
	else if ( income<=950000 ){
		tax=(float)( (300000*0.1)+(  (income-550000)*0.15  )  );
		if(tax<2000){
			tax=2000;
		}
	}
	else if ( income<=1250000 ) {
		tax=(float) (  (300000*0.1) + (400000*0.15)+(  (income-950000)*0.20  )  );
		if(tax<2000){
			tax=2000;
		}
	}
	else if( income>1250000 ){
		tax=(float) (  (300000*0.1) + (400000*0.15)+(300000*0.20)+(  (income-1250000)* 0.25  )  );
		if(tax<2000){
			tax=2000;
		}
	}

	return tax;
}

int decide(){
	char special,gender;
	int age;
start:
	
	do{
		fflush(stdin);
		printf("Are you a retarded or mentally challenged person (Y / N ) : ");
		scanf_s("%c",&special);
		printf("\nspecial=%d\n",special);
		if(special !=89 || special !=121 || special !=78 || special !=110 ){
			printf("\nInvalid Choice. Try again\n");
		}
	}while(special!=89 || special!=121 || special!=78 || special!=110 );

	do{
		fflush(stdin);
		printf("Please enter your gender( M / F ): ");
	scanf_s("%d",&gender);
			if(gender!=77 || gender!=109 || gender!=70 || gender!=102 ){
			printf("\nInvalid Choice. Try again\n");
		}
	}while(gender!=77 || gender!=109 || gender!=70 || gender!=102 );


	do{
		fflush(stdin);
		printf("Please enter your age: ");
		scanf_s("%d",&age);
		if(age<0){
			printf("\nINVALID CHOICE!! Please try again.\n");
		}
	}while(age<=0);

	if(special==89 || special==121){
		return 3;
	}
	else if(gender==70 || gender==102 || age>65){
		return 2;
	}
	else if( gender== 77 || gender== 109 ){
		return 1;
	}
	else{
		printf("\nINVALID!! Please try again.\n");
		goto start;
	}
}


