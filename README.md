# Income Tax Calculator

A simple console application that calculates income tax based upon inputs from the user.

## Simulating

To simulate, you must install all the software listed in the prerequisite section and then following the "How to simulate" section 

### Prerequisites

 * Visual Studio

### How-to-run
 
 * Clone the repo in your local directory
 * Start Visual studio and open the PL_tx_project.sln. Compile and run the project.
 * Once the console program starts, it will ask for various inputs from the user like income, age, gender etc. If valid inputs are provided it will calculate and display the income tax. 


## Author

* **[Abhijit Das](https://www.linkedin.com/in/abhijit-das-jd/)**


## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Gratitude to all my teachers, mentors, book writers and online article writers who provided me with the knowledge required to do the project. 

# SPECIAL Disclaimer

This program has been written years ago. It was my first actual program written in C. It may contain high level of funny code that may cause heart attack, brain stroke or suicidal thoughts for which I, the coder, will not take any responsibility of. 

